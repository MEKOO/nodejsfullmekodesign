
require('babel-register');
const express = require('express');
const morgan = require('morgan')('dev');
const config = require("./assets/config")
const swaggerDocument = require("./assets/swagger.json")
const bodyParser = require('body-parser');
const { success, error, checkAndChange }  = require("./assets/functions");
const mysql = require('promise-mysql');
const expressOasGenerator = require('express-oas-generator');
const swaggerUi = require('swagger-ui-express');
const cors = require("cors");

mysql.createConnection({
  host     : config.db.host,
  user     : config.db.user,
  password : config.db.password,
  database : config.db.database
}).then((db) => {

  console.log('connected as id ');
  const app = express();
  //swagger generator
  expressOasGenerator.init(app, {});
  // creation de la route express pour eviter d'ecrire la route à chaque fois
  let MembersRouter = express.Router();
  app.use(cors())
  app.use(morgan)
  app.use(express.json()) // for parsing application/json
  app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
  // swagger
  app.use(config.rootApi+'api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  let Members = require('./assets/classes/members_class')(db,config)

  MembersRouter.route('/:id')
    // recuperation d'un seul membres avec son ID
    .get( async (req, res) => {
        let member = await Members.getById(req.params.id)
        res.json(checkAndChange(member))
    })
    // modification d'un memebres avec son ID
    .put(async (req, res) => {
        // on va tester si le param body name existe
        let uapdateMembers = await Members.updateMembers(req.params.id, req.body.name)
        res.json(checkAndChange(uapdateMembers))
    })
    // delete le members avec son ID
    .delete( async (req, res) => {
      let deleteMembers = await Members.deleteMembers(req.params.id)
      res.json(checkAndChange(deleteMembers))
    })

    MembersRouter.route('/')
    // recuperation tous le membres avec un systeme de max option ? et query
    .get( async (req, res) => {
        let allMembers = await Members.getAll(req.query.max)
        res.json(checkAndChange(allMembers))
    })
    //ajoute un name avec post
    .post(async (req, res) => {
      // test si le name exist
      let addMembers = await Members.addMembers(req.body.name)
      res.json(checkAndChange(addMembers))
    })
  // route api
  app.use(config.rootApi+'members', MembersRouter)
  console.log(config.rootApi+'members','api url')
  app.listen(config.port, () => {
    console.log('Started on port'+ config.port);
  })

}).catch((err) => {
  console.log('Error during database connection')
  console.log(err.message)
})
