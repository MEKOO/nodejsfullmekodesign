
require('babel-register');
const express = require('express');
const morgan = require('morgan')('dev');
const config = require("./assets/config")
const bodyParser = require('body-parser');
const { success, error }  = require("./assets/functions");
const mysql      = require('mysql');


const db = mysql.createConnection({
  host     : config.db.host,
  user     : config.db.user,
  password : config.db.password,
  database : config.db.database
});

//on etablie la connection à db
db.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.message);
    return;
  }else {
    console.log('connected as id ' + db.threadId);
    const app = express();
    // creation de la route express pour eviter d'ecrire la route à chaque fois
    let MembersRouter = express.Router();
    app.use(morgan)
    app.use(express.json()) // for parsing application/json
    app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

    let Members = require('./assets/classes/members_class')(db,config)

    MembersRouter.route('/:id')
      // recuperation d'un seul membres avec son ID
      .get((req, res) => {
          db.query('SELECT * FROM members WHERE id = ?', [req.params.id], (error, results) => {
            if (error) {
              // error will be an Error if one occurred during the query
              res.json(error(error.message))
            }else {
              // results will contain the results of the query
              if (results[0] != undefined) {
                  res.json(success(results[0]))
              }else {
                res.json(error("wrong id"))
              }
            }
          });
      })
      // modification d'un memebres avec son ID
      .put((req, res) => {
          // on va tester si le param body name existe
          if (req.body.name) {
            db.query('SELECT * FROM members WHERE id = ?', [req.params.id], (err, results) => {
              if (err) {
                // error will be an Error if one occurred during the query
                res.json(error(err.message))
              }else {
                // results will contain the results of the query
                if (results[0] != undefined) {
                  // verifier si le name n'est pas deja pris
                  db.query('SELECT * FROM members WHERE name = ? AND id != ?', [req.body.name, req.params.id], (err, results) => {
                    if (err) {
                      res.json(error(err.message))
                    }else {
                      // test si name exist
                      if (results[0] != undefined) {
                        res.json(error('same name'))
                      } else {
                        db.query('UPDATE members SET name = ? WHERE id = ?', [req.body.name, req.params.id], (err, results) => {
                            if (err) {
                                res.json(error(err.message))
                            } else {
                              res.json(success(true))
                            }
                        })
                      }
                    }
                  })
                }else {
                  res.json(error("wrong id"))
                }
              }
            })
          } else {
            res.json(error('no name values'))
          }
      })
      // delete le members avec son ID
      .delete((req, res) => {
          db.query('SELECT * FROM members WHERE id = ?', [req.params.id], (error, results) => {
            if (error) {
              // error will be an Error if one occurred during the query
              res.json(error(error.message))
            }else {
              // results will contain the results of the query
              if (results[0] != undefined) {
                  db.query('DELETE FROM members WHERE id = ?', [req.params.id], (error, results) => {
                    if (error) {
                      // error will be an Error if one occurred during the query
                      res.json(error(error.message))
                    }else {
                      res.json(success(true))
                    }
                  })
              }else {
                res.json(error("wrong id"))
              }
            }
          });
      })

      MembersRouter.route('/')
      // recuperation tous le membres avec un systeme de max option ? et query
      .get((req, res) => {
          if (req.query.max != undefined && req.query.max > 0) {
            // verrifaication d'un params dans l'url max
            db.query('SELECT * FROM members LIMIT 0, ?', [req.query.max], (err, results) => {
              if (error) {
                // error will be an Error if one occurred during the query
                res.json(error(err.message))
              }else {
                // results will contain the results of the query
                res.json(success(results))
              }
            });
              // res.json(success(members.slice(0, req.query.max)))
          }else if(req.query.max != undefined) {
            //cas d'une eror
            res.json(error('wrong max value'))
          }
          else {
            db.query('SELECT * FROM members', (err, results) => {
              if (err) {
                // error will be an Error if one occurred during the query
                res.json(error(err.message))
              }else {
                // results will contain the results of the query
                console.log('test:',results)
                res.json(success(results))
              }
            });
          }
      })
      //ajoute un name avec post
      .post((req, res) => {
        // test si le name exist
        if (req.body.name) {
            // tester si un name est deja pris
            db.query('SELECT * FROM members WHERE name = ?', [req.body.name], (err, results) => {
              if (err) {
                res.json(error(err.message))
              } else {
                // on va tester si le results est vide ou pas.
                if (results[0] != undefined) {
                  res.json(error('name already taken'))
                } else {
                    // ajout d ela variable members avec id et name
                  db.query('INSERT INTO members(name) VALUES(?)', [req.body.name], (err, results) => {
                    if (err) {
                      res.json(error(err.message))
                    }else {
                        // req en prenenat en compte le name et id pour auto-incremente
                        db.query('SELECT * FROM `members` WHERE `name` = ?', [req.body.name], (err, results) => {
                          console.log('messages post continue', [req.body.name])
                          if (err) {
                            res.json(error(err.message))
                          } else {
                            // on ajoute le membre name & id
                            console.log('add membres id name', results[0].id, results[0].name)
                            res.json(success({
                              id: results[0].id,
                              name: results[0].name
                            }))
                          }
                        })
                    }
                  })
                }
              }
            })

        }else {
          // si non error
          res.json(error('no name value'))
        }
      })
    // route api
    app.use(config.rootApi+'members', MembersRouter)
    console.log(config.rootApi+'members','api url')
    app.listen(config.port, () => {
      console.log('Started on port'+ config.port);
    })
  }
});
