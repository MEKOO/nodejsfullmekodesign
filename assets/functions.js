exports.success = (result) => {
  return {
    status: 'success',
    result: result
  }
}

exports.error = (error) => {
  return {
    status: 'error',
    messages: error
  }
}
exports.isErr = (err) => {
  return err instanceof Error;
}
exports.checkAndChange = (obj) => {
  if (this.isErr(obj)) {
    return this.error(obj.message)
  }else {
    return this.success(obj)
  }
}
