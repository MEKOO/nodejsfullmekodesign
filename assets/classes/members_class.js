let db, config
    // creation de modules nodejs

module.exports = (_db, _config) => {
    db = _db
    config = _config
    return Members
};

let Members = class {
    // stattic params
    // get one id promise
    static getById(id) {
        return new Promise((next) => {
            db.query('SELECT * FROM members WHERE id = ?', [id])
                .then((results) => {
                    // results will contain the results of the query
                    if (results[0] != undefined) {
                        next(results[0])
                    } else {
                        next(new Error(config.errors.wrondId))
                    }
                }).catch((error) => {
                    next(error)
                })
        })
    }

    //get all  members
    static getAll(max) {
        return new Promise((next) => {
            if (max != undefined && max > 0) {
                // verrifaication d'un params dans l'url max
                db.query('SELECT * FROM members LIMIT 0, ?', [parseInt(max)])
                    .then((results) => {
                        return next(results)
                    }).catch((err) => {
                        return next(err)
                    })
                    // res.json(success(members.slice(0, req.query.max)))
            } else if (max != undefined) {
                //cas d'une eror
                next(new Error(config.errors.wrondMaxValue))
            } else {
                db.query('SELECT * FROM members')
                    .then((results) => {
                        return next(results)
                    }).catch((err) => {
                        return next(err)
                    })
            }
        })
    }

    //post members
    static addMembers(name) {
            return new Promise((next) => {
                if (name != undefined && name.trim() != '') {
                    // tester si un name est deja pris
                    name = name.trim()
                    db.query('SELECT * FROM members WHERE name = ?', [name])
                        .then((results) => {
                            // on va tester si le results est vide ou pas.
                            if (results[0] != undefined) {
                                next(new Error(config.errors.wrondNameTaken))
                            } else {
                                // ajout d ela variable members avec id et name
                                return db.query('INSERT INTO members(name) VALUES(?)', [name])

                            }
                        })
                        .then(() => {
                            // req en prenenat en compte le name et id pour auto-incremente
                            return db.query('SELECT * FROM `members` WHERE `name` = ?', [name])
                        })
                        .then((results) => {
                            next({
                                id: results[0].id,
                                name: results[0].name
                            })
                        })
                        .catch((err) => {
                            return next(err)
                        })
                } else {
                    // si non error
                    next(new Error(config.errors.wrondNoNameValue))
                }
            })
        }
        // put
    static updateMembers(id, name) {
        return new Promise((next) => {
            if (name != undefined && name.trim() != '') {
                name = name.trim()
                db.query('SELECT * FROM members WHERE id = ?', [id])
                    .then((results) => {
                        // results will contain the results of the query
                        if (results[0] != undefined) {
                            // verifier si le name n'est pas deja pris
                            return db.query('SELECT * FROM members WHERE name = ? AND id != ?', [name, id])
                        } else {
                            next(new Error(config.errors.wrongId))
                        }
                    })
                    .then((results) => {
                        // test si name exist
                        if (results[0] != undefined) {
                            next(new Error(config.erros.wrondSameName))
                        } else {
                            return db.query('UPDATE members SET name = ? WHERE id = ?', [name, id])
                        }
                    })
                    .then(() => next(true))
                    .catch((err) => next(err))
            } else {
                next(new Error(config.errors.wrondNoNameValue))
            }

        })
    }

    //delete members
    static deleteMembers(id) {
        return new Promise((next) => {
            db.query('SELECT * FROM members WHERE id = ?', [id])
                .then((results) => {
                    if (results[0] != undefined) {
                        return db.query('DELETE FROM members WHERE id = ?', [id])
                    } else {
                        next(new error("wrong id"))
                    }
                })
                .then((results) => next(true))
                .catch((err) => next(err))
        })
    }
}