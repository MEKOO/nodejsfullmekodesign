require('babel-register');
const express = require('express');
const morgan = require('morgan');
const config = require("./config")
const bodyParser = require('body-parser');
const { success, error } = require("./functions");
const app = express();

const members = [{
            id: 1,
            name: 'John'
        },
        {
            id: 2,
            name: 'Julie'
        },
        {
            id: 3,
            name: 'Jack'
        }
    ]
    // creation de la route express pour eviter d'ecrire la route à chaque fois
let MembersRouter = express.Router();
app.use(morgan('dev'))
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

MembersRouter.route('/:id')
    // recuperation d'un seul membres avec son ID
    .get((req, res) => {
        let index = getIndex(req.params.id);
        if (typeof(index) == 'string') {
            // si pas membres error
            res.json(error(index))
        } else {
            //on recupère bien un membres
            res.json(success(members[index]));
        }
    })
    // modification d'un memebres avec son ID
    .put((req, res) => {
        let index = getIndex(req.params.id);
        if (typeof(index) == 'string') {
            res.json(error(index))
        } else {
            let same = false;
            for (var i = 0; i < members.length; i++) {
                if (req.body.name == members[i].name && req.params.id != members[i].id) {
                    // alors on a un problème de modification
                    same = true
                    break
                }
            }

            if (same) {
                res.json(error('same name'))
            } else {
                members[index].name = req.body.name
                res.json(success(true))
            }
        }
    })
    // delete le members avec son ID
    .delete((req, res) => {
        let index = getIndex(req.params.id);
        if (typeof(index) == 'string') {
            res.json(error(index))
        } else {
            members.splice(index, 1)
            res.json(success(members))
        }
    })
MembersRouter.route('/')
    // recuperation tous le membres avec un systeme de max option ? et query
    .get((req, res) => {
        if (req.query.max != undefined && req.query.max > 0) {
            // verrifaication d'un params dans l'url
            res.json(success(members.slice(0, req.query.max)))
        } else if (req.query.max != undefined) {
            //cas d'une eror
            res.json(error('wrong max value'))
        } else {
            res.json(success(members))
        }
    })
    //ajoute un name avec post
    .post((req, res) => {
        // test si le name exist
        if (req.body.name) {
            // tester si un name est deja pris
            let sameName = false;
            for (var i = 0; i < members.length; i++) {
                if (members[i].name === req.body.name) {
                    sameName = true
                    break

                }
            }
            if (sameName) {
                res.json(error('name already taken'))
            } else {
                let member = {
                    id: createId(),
                    name: req.body.name
                }
                members.push(member);
                res.json(success(member))
            }
        } else {
            // si non error
            res.json(func.error('no name value'))
        }
    })
    // route api
app.use(config.rootApi + 'members', MembersRouter)

app.listen(config.port, () => {
    console.log('Started on port' + config.port);
})


function getIndex(id) {
    for (var i = 0; i < members.length; i++) {
        if (members[i].id == id) {
            return i
        }
    }
    // id n'est pas bon
    return 'wrong id';
};

function createId() {
    let lastMember = members[members.length - 1].id + 1;
    return lastMember;
};