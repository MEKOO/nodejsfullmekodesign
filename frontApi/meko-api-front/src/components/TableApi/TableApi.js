import React from 'react';
import { Wrapper } from './Table.styles'

function TableApi (props){
  return (
    <Wrapper striped bordered hover>
      <thead>
        <tr>
          <th>{props.index}</th>
          <th>{props.value}</th>
        </tr>
      </thead>
      <tbody>
          <tr>
            {props.item}
          </tr>
      </tbody>
    </Wrapper>
  )
}
export default TableApi;
