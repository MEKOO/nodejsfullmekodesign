import styled from 'styled-components';
import { Table } from 'react-bootstrap';

export const Wrapper =  styled(Table)`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  padding: 0.5em 1em;
  color:red;
`
