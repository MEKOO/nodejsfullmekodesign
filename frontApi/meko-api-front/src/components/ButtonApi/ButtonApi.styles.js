
import { Button } from 'react-bootstrap';
import styled from 'styled-components';

export const Wrapper =  styled(Button)`
  background: transparent;
  border-radius: 3px;
  border: 2px solid palevioletred;
  color: palevioletred;
  margin: 0.5em 1em;
  padding: 0.5em 1em;
  ${props => props.className ? `
    background: palevioletred; color:white;
      &:hover {
        background: palevioletred;
        border:none;
      }` :
    `background: red; color: white`}
`
