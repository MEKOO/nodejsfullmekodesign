import React from 'react';
import PropTypes from 'prop-types';
import { Wrapper } from './ButtonApi.styles'

function ButtonApi(props) {
  return (
    <Wrapper className={props.className} onClick={props.onClick}>{props.name}</Wrapper>
  )
}

ButtonApi.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  onclick: PropTypes.func,
}

export default ButtonApi;
