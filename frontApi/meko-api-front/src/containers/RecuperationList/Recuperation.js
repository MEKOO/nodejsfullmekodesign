import React, { useState, useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { fetchMembers } from './recuperation.action';
import ButtonApi from "../../components/ButtonApi";
//import TableApi from "../../components/TableApi";



function Recuperation(props) {
  const [data, setData] = useState([]);

  // Similaire à componentDidMount et componentDidUpdate :
  useEffect(() => {
      // Met à jour  l’API.
      props.fetchMembersAll();

  },[]);

  const handlerClick = () => {
    let membershow = props.members.messagesSucess;
    if (membershow !== undefined) {
      return membershow.result.map((item) => (item = <span>{item.name}</span>));
    }
  };

  return (
    <div>
      <ButtonApi
        name="clique pour voir les tous membres"
        className="primary"
        onClick={() => setData(handlerClick())}
      />
      <h4>{`mon tableau de membres: ${data.map(
        (item) => item.props.children
      )}`}</h4>
    </div>
  );
}


const mapStateToProps = (state) => ({
  members: state.recupMember.members,
});

const mapDispatchToProps = (dispatch) => ({
  fetchMembersAll: bindActionCreators(fetchMembers, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Recuperation);



