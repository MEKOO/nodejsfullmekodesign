import { RECUPERATION } from './constants';

export const initialState = {
         members: [],
         isFetching: false,
         status: "",
         error:""
       };

const recupMember =  (state = initialState, action) =>  {
    switch (action.type) {
      case RECUPERATION:
        return Object.assign({}, state, {
          members: action,
          isFetching:action.isFetching,
          statuts: action.statuts,
          error:action.error
        });
      default:
        return state;
    }
}

export default recupMember;