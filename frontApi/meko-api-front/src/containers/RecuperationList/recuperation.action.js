import axios from 'axios';
import { RECUPERATION } from './constants';

export const recuperation = (messagesSucess) => ({
        type: RECUPERATION,
         messagesSucess,
         receivedAt: Date.now(),
       });

const urlApi = "http://localhost:8080/api/v1/members";
const config = {
  headers: {
        "Content-Type": "application/x-www-form-urlencoded", 
        "Access-Control-Allow-Origin": "http://localhost:3000" 
    }
};

export const fetchMembers = () => dispatch => {
         axios({
           method: "get",
           url: urlApi,
           config,
           responseType: "stream",
         })
           .then(function (response) {
             dispatch(recuperation(response.data));
           })
           .catch((err) => err.message);
       };
