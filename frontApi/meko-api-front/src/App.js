import React from 'react';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Recuperation from './containers/RecuperationList/Recuperation';

function App() {
  return (
    <div className="App">
        <Recuperation/>
    </div>
  );
}

export default App;
