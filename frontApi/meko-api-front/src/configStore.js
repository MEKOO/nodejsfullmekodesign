/* eslint-disable consistent-return */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-underscore-dangle */
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddelware from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import recupMember from "./containers/RecuperationList/reducerRecup";

const rootReducer = combineReducers({
  recupMember: recupMember,
});

export default function configureStore(initialState, history) {
  let composeEnhancers = compose;
  const middlewares = [thunkMiddelware, routerMiddleware(history)];
  const enhancers = [applyMiddleware(...middlewares)];

  if (process.env.NODE_ENV && typeof window === 'object') {
    if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({});
  }

  const createStoreWithMiddelware = createStore(
    rootReducer,
    initialState,
    composeEnhancers(...enhancers),
  );

  if (module.hot) {
    module.hot.accept(rootReducer, () => {
      createStoreWithMiddelware.replaceReducer(rootReducer);
    });
  }
  return createStoreWithMiddelware;
}
